package main;

import java.util.ArrayList;

import lib.OrderLine;

public class OrderLineDemo {

	public static void main(String[] args) {

		ArrayList<OrderLine> myLine = new ArrayList<>();
		myLine.add(new OrderLine("juice", 20, 10));
		myLine.add(new OrderLine("chug jug", 100, 5));
		myLine.add(new OrderLine("bunny", 400, 1));
		myLine.add(new OrderLine("doggy", 400, 1));
		myLine.add(new OrderLine("pizza", 1400, 40));

		// 5.6.b
		int runningCost = 0;

		for (OrderLine n : myLine) {
			System.out.println(n.toString());
			runningCost = runningCost + n.getCost();
			System.out.println(runningCost);
		}

		// 5.6.b
		// printing the average cost of each orderline
		double avCost = runningCost / myLine.size();
		System.out.println("the average of each orderline is:" + avCost);

		// Printing the number of OrderLines
		System.out.println("the number of orderlines is: " + myLine.size());

		// Printing the sum of the orderlines using stream
		double totalSum = myLine.stream() // generate a stream
				.mapToInt(n -> n.getCost()) // generate a stream of the cost of
											// each orderline
				.sum(); // adding the those cost together

		System.out.println(totalSum);

	}

}
