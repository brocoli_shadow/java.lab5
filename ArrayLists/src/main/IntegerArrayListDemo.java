package main;

//need to import ArrayList from within java.util package
import java.util.ArrayList; 

public class IntegerArrayListDemo {

	public static void main(String[] args) {
		//create an ArrayList of numbers using the Integer wrapper class
		//(wrapper class instantiate primitives into objects)
		//Here int into Integer
		ArrayList<Integer> list = new ArrayList<Integer>();
		//print list, should be empty
		System.out.println(list.toString());

		//populate with some numbers
		//Appends the specified element to the end of this list.
		list.add(8);  //auto-boxing, actually: add(new Integer(8)) 
		list.add(32); //auto-boxing ie int into Integer
		list.add(5);  //Unboxing is the opposite
		
		//print list contents
				System.out.println(list.toString());

		//using the other add method
		//Inserts the specified element at the specified position in this list		
		list.add(1,5);  //inserts 5 at index 1
		list.add(0,104);  //inserts 104 at index 0

		//print list contents
		System.out.println(list.toString());



		//output the current size of the list
		//Returns the number of elements in this list.
		System.out.println("The size is: " + list.size());


		//print element at index 0 using get method
		//Returns the element at the specified position in this list.
		System.out.println("First element is: " + list.get(0));


		//searches for item (5) and returns index
		//Returns the index of the first occurrence of the specified element in this list,
		//or -1 if this list does not contain the element.
		int pos = list.indexOf(5);
		System.out.println("Index of 5: (expected 2) result is "  + pos);


		//use contains() to check whether some number is in the list
		if (list.contains(104)) {
			System.out.println("The list contains 104"); //should print this
		}
		else {
			System.out.println("The list does NOT contains 104");
		}
		
		
		//print using a for-i loop with size() and get(i)
		System.out.println("\nUsing standard for-i loop");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		//print using for-each loop
		System.out.println("Using for-each loop");
		for (Integer x : list) {
			System.out.println(x);
		}

		//print using forEach method (added in Java 8)
		System.out.println("Using forEach method");
		//passes a lambda expression to the method
		list.forEach(x -> System.out.println(x));


		//TEST OTHER METHODS HERE...
		
		//Overloaded method remove(int index)
		System.out.println("Overloaded method remove(int index)");
		list.remove(1);
		list.forEach(x -> System.out.println(x));
		
		//Set(int index, E element) method
		//Replaces the element at the specified position in this list with the specified element.
		System.out.println("Set(int index, E element) method");
		list.set(2, 33);
		list.forEach(x -> System.out.println(x));
		
		//public void clear()
		//Removes all of the elements from this list. The list will be empty after this call returns.
		System.out.println("clear() method");
		list.clear();
		list.forEach(x -> System.out.println(x));
		
		//isEmpty()
		//Returns true if this list contains no elements
		System.out.println("isEmpty() method Returns true if this list contains no elements");
		System.out.println(list.isEmpty());
		
	}

}
