package main;

import java.util.ArrayList;

public class StringListDemo {

	public static void main(String[] args) {
		// Creating an qrray list of strings
		ArrayList<String> list2 = new ArrayList<String>();

		// Populate the list with Strings
		list2.add("Orange");
		list2.add("Banana");
		list2.add("kiwi");
		list2.add("Pineapple");
		list2.add("Melon");

		// Printing the list using foreach method
		list2.forEach(x -> System.out.println(x));
		System.out.println("\n");

		// Replacing an entry
		list2.set(1, "cauliflower");

		// Printing the list using foreach method
		list2.forEach(x -> System.out.println(x));
		System.out.println("\n");

		// Removing a particular entry from the list
		list2.remove(4);

		// Printing the list using foreach method
		list2.forEach(x -> System.out.println(x));
		System.out.println("\n");

		// Using a for-each loop to print all entries in upper case
		for (String x : list2) {
			System.out.println(x.toUpperCase());
		}
		System.out.println("\n");

		// Using the forEach loop to print all entries in lower case
		list2.forEach(y -> System.out.println(y.toLowerCase()));
		System.out.println("\n");

	}

}
