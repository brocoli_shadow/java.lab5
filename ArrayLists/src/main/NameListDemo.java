package main;

import lib.Name;
import java.util.*;

public class NameListDemo {

	public static void main(String[] args) {
		// creating an arrayList of objects Name
		List<Name> register = new ArrayList<>();

		// creating scanner to take user input
		Scanner sc = new Scanner(System.in);

		// Prompting user for the input of 4 entry for name objects
		for (int i = 0; i <= 3; i++) {
			System.out.println("Enter a firstName: ");
			String firstName = sc.next();
			System.out.println("Enter a surname: ");
			String surname = sc.next();
			register.add(new Name(firstName, surname));
		}
		// close the scanner
		sc.close();

		// Print the register using forEach loop
		System.out.println("Printing the newly populated list");
		register.forEach(x -> System.out.println(x.toString()));

		// checking for the presence of an entry
		boolean exists = register.stream().anyMatch(nm -> nm.getFamilyName().equals("Smith"));
		System.out.println(exists);

		// 5.6e
		// Print entries with surname of more than 10 characters
		register.stream() // stream the collection
				.filter(y -> y.getFamilyName().length() > 10) // only entries
																// with long
																// surnames
				.forEach(x -> System.out.println(x.toString()));// print each of
																// filtered
																// entries
		
		//Returns true if this list contains the specified element. 
		//More formally, returns true if and only if this list contains
		//at least one element e such that (o==null ? e==null : o.equals(e)).
		//public boolean contains(Object o)
		
		Name compared = new Name("Nadia","Boukraa");
		boolean testcontains = register.contains(compared);
		System.out.println(testcontains);
		
	}

}
